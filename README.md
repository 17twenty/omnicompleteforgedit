Omnicomplete For Gedit
=======================

The goal of this project is to bring my favourite feature of Vim to Gedit namely 
autocomplete/omnicomplete.

This work is based off of the Gtk2/PyGTK code by Osmo Salomaa, Michael McDonnell,
Rodrigo Pinheiro Marques de Araujo and is in the early stage of development but
if anyone wants to hack on it - please feel free!
